#! /usr/bin/python

"""@package main
@brief Main package.
@date 2021-01-3 Sun 04:58 PM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2021 Sangsoic
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from Modules.constants import *
from Modules.compiler import *
from Modules.lsystem import *
from Modules.io import *

##
# @brief Main function.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 05:01 PM
# @param argv List of string containing arguments passed from command line.
def main(argv) :
    sourcein, pathin, pathout, verbose, run = get_param(argv);
    plain = loadin(pathin) if pathin else sourcein;
    if plain :
        lsystem = LSystem(**parse(lex(plain)));
        enctable = lsystem.update_table(TURTLE_ENCODING_TABLE);
        script = lsystem.transcode(enctable);
        if pathout :
            lsystem.export(pathout, script);
        if verbose :
            print(script);
        if run:
            exec(script, globals(), globals());
    else :
        raise_and_exit(ERROR_MSGS[11], os.EX_DATAERR);

if __name__ == "__main__" :
    main(sys.argv);

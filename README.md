# L-System compiler.

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

This project consists of python program that compiles deterministic L-System scripts into python turtle code. 
*Next possible updates could include other types of L-Systems Ex : Stochastic, Parametric, Context Sensitive...*

## L-System script syntax

### Attributes

Words | Meaning
----- | -------
axiom | Level 0 instruction(s).
rules | Production rules
angle | Turning angle
rules | Line length
level | Level recursion

### Production Rules symbols

Character | Meaning
--------- | -------
a         | Move forward by line length drawing a line
b         | Move forward by line length without drawing a line
\+        | Turn right by turning angle
\-        | Turn right by turning angle
\*        | Reverse direction (ie: turn by 180 degrees)
[         | Push current drawing state onto stack
]         | Pop current drawing state from the stack and restore that state
[c-z]     | Dummy symbols.

## Example

Let's look at the TestFiles/figure5 simple L-System script.

```
axiom = "----x"
rules = "a=aa" 
        "x=a+[-a-xa-x][+aa][--xa[+x]][++a-x]"
angle = 25
length = 10
level = 4
```

We then execute the following command.

```Python
./main.py < TestFiles/figure5 > figure5_turtled.py
```

And when we execute the resulting python turtle script we get.

![figure5 at level 4](Img/examplef5.png)

## Other Examples

![figure0 at level 3](Img/examplef0.png)

![figure1 at level 4](Img/examplef1.png)

![figure3 at level 4](Img/pp.png)

![figure4 at level 3](Img/examplef4.png)






"""@package io
@brief Routines related IO.
@date 2021-01-3 Sun 09:58 PM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2021 Sangsoic
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os, sys
import re
from Modules.constants import *

##
# @brief Raises and exits error code and print out messages on stderr.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 10:20 PM
# @param msg Message to print out.
# @param code Standard code to return.
def raise_and_exit(msg, code) :
    sys.stderr.write(msg + '\n');
    sys.exit(code);

##
# @brief Reads stdin till EOF.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 10:23 PM
# @return stdin content.
def read_stdin() :
    try :
        contentin = sys.stdin.read();
    except KeyboardInterrupt :
        sys.exit(os.EX_IOERR);
    return contentin;

##
# @brief Gets parameters/arguments from command line.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 10:30 PM
# @param argv List of string containing arguments passed from command line.
# @return Parameters/Arguments from command line.
def get_param(argv) :
    argvStr = ' ' + " ".join(argv[1:]);
    if re.fullmatch(r'^\s?( -i \S+)?( -o \S+)?( -[sr]){0,3}$', argvStr) :
        sourcein, pathin = ("", argv[argv.index("-i")+1]) if "-i" in argv else (read_stdin(), "");
        pathout = argv[argv.index("-o")+1] if "-o" in argv else "";
        verbose = "-s" not in argv;
        run = "-r" in argv;
    else :
        raise_and_exit(ERROR_MSGS[0], os.EX_USAGE);
    return sourcein, pathin, pathout, verbose, run;

##
# @brief Loads input file.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 10:58 PM
# @param pathin input path.
# @return Parameters/Arguments from command line.
def loadin(pathin) :
    if os.access(pathin, os.R_OK) :
        with open(pathin, 'r') as filein:
            contentin = filein.read();
    else :
        raise_and_exit(ERROR_MSGS[1], os.EX_NOINPUT);
    return contentin;

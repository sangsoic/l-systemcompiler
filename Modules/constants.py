"""@package constants
@brief All constants used across the whole program.
@date 2021-01-3 Sun 08:00 PM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2021 Sangsoic
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import re

KEYWORDS = ("axiom", "rules", "angle", "length", "level");

KEYCHARS = re.escape("ab+-*][" + "".join((chr(c) for c in range(99,123))));

TURTLE_ENCODING_TABLE = {
'a' : "pd();fd(%.2f);",
'b' : "pu();fd(%.2f);",
'+' : "right(%.3f);",
'-' : "left(%.3f);",
'*' : "right(180);",
'[' : "s.append((pos(),heading()));",
']' : "pu();h0,h1=s.pop();goto(*h0);setheading(h1);"
};

ERROR_MSGS = (
"help: correct usage : ./main.py [-i PATH] [-o PATH] [-s] [-r]" + \
"""
OPTIONS :
-h\thelp menu.
-i\tinput file path. (if not mentioned, reads from stdin)
-o\toutput file path.
-s\tdoes not print out generated turtle script on stdout. (faster)
-r\trun turtle script right after generation.""",
"error: cannot open given input file: do not exist or not enough permission.",
"error: bad syntax found in given input: invalid attribute.",
"error: bad syntax found in given input: missing operator.",
"error: bad syntax found in given input: invalid operator.",
"error: bad syntax found in given input: missing attribute.",
"error: bad syntax found in given input: invalid rule.",
"error: bad syntax found in given input: %s, %s, %s have to be integers." % KEYWORDS[2:5],
"error: bad syntax found in given input: invalid axiom.",
"error: bad syntax found in given input: missing value.",
"error: cannot open given output file: not enough permission.",
"error: no instructions given."
);
